import play.Project._

name := "guideme"

version := "1.0"

libraryDependencies ++= Seq(
  cache,
  "org.webjars" %% "webjars-play" % "2.2.2",
  "com.typesafe.play" %% "play-slick" % "0.6.0.1",
  "com.typesafe.slick" %% "slick" % "2.0.2",
  "joda-time" % "joda-time" % "2.3",
  "org.joda" % "joda-convert" % "1.5",
  "com.github.tototoshi" %% "slick-joda-mapper" % "1.1.0",
  "mysql" % "mysql-connector-java" % "5.1.21",
  "com.newzly"  %% "phantom-dsl" % "0.8.0"
)

playScalaSettings