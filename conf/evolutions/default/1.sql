# --- Created by Slick DDL
# To stop Slick DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table `MERCHANT` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`name` VARCHAR(254) NOT NULL,`description` VARCHAR(254) NOT NULL,`site` VARCHAR(254));
create table `PROPOSAL` (`id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,`subject` VARCHAR(254) NOT NULL,`description` VARCHAR(254) NOT NULL,`terms` VARCHAR(254),`discount` INTEGER,`price` DECIMAL(21,2),`expires` TIMESTAMP NOT NULL,`fenceId` BIGINT NOT NULL,`merchantId` BIGINT NOT NULL);

# --- !Downs

drop table `MERCHANT`;
drop table `PROPOSAL`;

