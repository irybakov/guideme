package models.feed

import java.util.UUID

import com.datastax.driver.core.{ResultSet, Row}
import com.newzly.phantom.Implicits._
import com.newzly.phantom.iteratee.Iteratee
import models.cassandra.DBConnector
import org.joda.time.DateTime

import scala.concurrent.{Future => ScalaFuture}

/**
 * Created by irybakov on 6/15/14.
 */

case class Feed(
                 id: UUID,
                 user_id: String,
                 offer_id: Long,
                 updated: DateTime,
                 proposal: Map[String, String],
                 place: Map[String, String],
                 merchant: Map[String, String]
                 )

sealed class FeedProposals extends CassandraTable[FeedProposals, Feed] {

  object id extends UUIDColumn(this) with PartitionKey[UUID] {

  }

  object user_id extends StringColumn(this)

  object offer_id extends LongColumn(this)

  object updated extends DateTimeColumn(this)

  object proposal extends MapColumn[FeedProposals, Feed, String, String](this)

  object place extends MapColumn[FeedProposals, Feed, String, String](this)

  object merchant extends MapColumn[FeedProposals, Feed, String, String](this)


  // Now the mapping function, transforming a row into a custom type.
  // This is a bit of boilerplate, but it's one time only and very short.
  def fromRow(row: Row): Feed = {
    Feed(
      id(row),
      user_id(row),
      offer_id(row),
      updated(row),
      proposal(row),
      place(row),
      merchant(row)
    )
  }
}

  object FeedProposals extends FeedProposals with DBConnector {
    override lazy val tableName = "proposal_feed"

    // now define a session, a normal Datastax cluster connection
    //implicit val session = DBConnector.session;

    // inserting proposal to the feed
    def insertNewRecord(proposal: Feed): ScalaFuture[ResultSet] = {
      insert.value(_.id, proposal.id)
        .value(_.user_id, proposal.user_id)
        .value(_.offer_id, proposal.offer_id)
        .value(_.updated, proposal.updated)
        .value(_.proposal, proposal.proposal)
        .value(_.place, proposal.place)
        .value(_.merchant, proposal.merchant)
        .future()
    }


    def getFeedPage(start: UUID, limit: Int): ScalaFuture[Seq[Feed]] = {
      select.where(_.id gtToken start).limit(limit).fetch()
    }


    // The fetchEnumerator method is the real power behind the scenes.
    // You can retrieve a whole table, even with billions of records, in a single query.
    // Phantom will collect them into an asynchronous, lazy iterator with very low memory foot print.
    // Enumerators, iterators and iteratees are based on Play iteratees.
    // You can keep the async behaviour or collect through the Iteratee.
    def getEntireTable: ScalaFuture[Seq[Feed]] = {
      select.fetchEnumerator() run Iteratee.collect()
    }

  }


