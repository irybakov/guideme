package models.cassandra

/**
 * Created by irybakov on 6/15/14.
 */


import scala.concurrent. { blocking, Future }
import com.datastax.driver.core.{Row, Cluster, Session}
import com.newzly.phantom.Implicits._

object DBConnector {
  val keySpace = "guideme"

  lazy val cluster = Cluster.builder()
    .addContactPoint("54.187.98.173")
    .withPort(9042)
    .withoutJMXReporting()
    .withoutMetrics()
    .build()

  lazy val session = blocking {
    cluster.connect(keySpace)
  }
}

trait DBConnector {
  self: CassandraTable[_, _] =>

  def createTable(): Future[Unit] ={
    create.future() map (_ => ())
  }

  implicit lazy val datastax: Session = DBConnector.session
}
