package models

import play.api.db.slick.Config.driver.simple._
import scala.math._
import org.joda.time.{Period, DateTime}
import com.github.tototoshi.slick.MySQLJodaSupport._
import models.paging.Page
import scala.slick.lifted

case class Proposal(
                     id: Option[Long],
                     subject: String,
                     description: String,
                     terms: String,
                     discount: Int,
                     price: BigDecimal,
                     expires: DateTime,
                     fenceId: Long,
                     merchantId: Long) {
   lazy val economy = price * discount / 100
   lazy val effectivePrice = price - economy
   lazy val image = "http://80.241.38.38:9000/img/proposal/" + id.get  +".png"
   def timeLeft = new Period(DateTime.now(), expires)
}


/**
 * Created by irybakov on 4/19/14.
 */
class Proposals(tag: Tag) extends Table[Proposal](tag, "PROPOSAL") {

  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def subject = column[String]("subject", O.NotNull)
  def description = column[String]("description", O.NotNull)
  def terms = column[String]("terms", O.Nullable)

  def discount = column[Int]("discount", O.Nullable)
  def price = column[BigDecimal]("price", O.Nullable)

  def expires = column[DateTime]("expires", O.NotNull)

  def fenceId = column[Long]("fenceId", O.NotNull)
  def merchantId = column[Long]("merchantId", O.NotNull)

  def * = (id.?, subject,description,terms,discount,price,expires,fenceId,merchantId) <> (Proposal.tupled, Proposal.unapply)

  def merchant = foreignKey("Merchamnt_FK",merchantId,MerchantsService.merchants)(_.id)

}


object ProposalsService {
  //create an instance of the table
  val proposals = TableQuery[Proposals] //see a way to architect your app in the computers-database-slick sample

  def all()(implicit s: Session): List[Proposal] = {proposals.list}

  def findById (id: Long)(implicit s: Session): Proposal = {
    val proposalByID  = proposals.withFilter(_.id === id)

    proposalByID.first()
  }

  def findByIdwithMerchant(id: Long)(implicit s: Session): (Proposal,Merchant) = {
    val proposalByID: lifted.Query[(Proposals, Merchants), (Proposal, Merchant)] = for {
      p <- proposals.withFilter(_.id === id)
      m <- p.merchant
    } yield (p,m)
    //val proposalByID  = proposals.withFilter(_.id === id)

    //println("SQL" + proposalByID.selectStatement)
    proposalByID.list().last
  }

  def join()(implicit s: Session): List[(Proposal, Merchant)] = {
    val joinQuery: lifted.Query[(Proposals, Merchants), (Proposal, Merchant)] = for {
      p <- proposals
      m <- p.merchant
    } yield (p,m)

    //println("SQL" + joinQuery.selectStatement)
    joinQuery.list
  }

  def insert(proposal: Proposal)(implicit s: Session) {
    proposals.insert(proposal)
  }

  /**
   * Count all proposals without a filter
   */
  def count()(implicit s: Session): Int =
    Query(proposals.length).first


  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%")(implicit s: Session): Page[Proposal] = {

    val offset = pageSize * page

    val query =
      (for {
        proposal <- proposals
      } yield (proposal))
        .drop(offset)
        .take(pageSize)

    val totalRows = count()
    val pageCount = abs(totalRows / pageSize) + 1
    val result = query.list

    Page(result, page, offset, pageCount, totalRows)
  }

  def listByMerchant(merchantId: Long, offset: Int = 0, pageSize: Int = 10)(implicit s: Session): List[(Proposal, Merchant)] = {

    val query =
      (for {
        p <- proposals.withFilter(_.merchantId === merchantId)
        m <- p.merchant
      } yield (p,m))
        .drop(offset)
        .take(pageSize)

    //println("SQL" + query.selectStatement)
    query.list
  }

}