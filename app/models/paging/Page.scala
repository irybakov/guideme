package models.paging

/**
 * Created by irybakov on 5/25/14.
 */
case class Page[A](items: Seq[A], page: Int, offset: Long, pageCount: Int, total: Long) {
  lazy val prev = Option(page - 1).filter(_ >= 0)
  lazy val next = Option(page + 1).filter(_ => (offset + items.size) < total)
  lazy val idx = Option(page).filter(_ < pageCount)
}


