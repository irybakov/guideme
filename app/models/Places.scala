package models

import org.joda.time.{Period, DateTime}
import play.api.db.slick.Config.driver.simple._
import scala.math._
import scala.slick.lifted

/**
 * Created by irybakov on 6/15/14.
 */
case class Place(
                  id: Option[Long],
                  merchantId: Long,
                  latitude: Double,
                  longitude: Double,
                  fanceId: Long,
                  name: String,
                  address: String)

class Places (tag: Tag) extends Table[Place](tag, "PLACES") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def merchantId = column[Long]("merchantId", O.Nullable)


  def latitude = column[Double]("latitude", O.NotNull)

  def longitude = column[Double]("longitude", O.NotNull)

  def fenceId = column[Long]("fenceId", O.NotNull)

  def name = column[String]("name", O.NotNull)

  def address = column[String]("address", O.Nullable)

  def * = (id.?, merchantId, latitude,longitude,fenceId,name,address) <> (Place.tupled, Place.unapply)

  def merchant = foreignKey("Merchamnt_FK",merchantId,MerchantsService.merchants)(_.id)
}

object PlacesService {
  val places = TableQuery[Places]

  def findByFenceId(fenceId: Long)(implicit s: Session): List[(Proposal, Merchant, Place)] = {
    val proposalByFenceID: lifted.Query[(Proposals, Merchants, Places), (Proposal, Merchant, Place)] = for {
      place <- places.withFilter(_.fenceId === fenceId)
      m <- place.merchant
      p <-  ProposalsService.proposals.withFilter(_.merchantId === m.id)
    } yield (p,m,place)

    //println("SQL" + proposalByFenceID.selectStatement)
    //val proposalByFenceID  = proposals.withFilter(_.fenceId === id)
    proposalByFenceID.list
  }
}
