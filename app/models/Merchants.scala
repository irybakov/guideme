package models

import org.joda.time.{Period, DateTime}
import play.api.db.slick.Config.driver.simple._
import scala.math._

import com.github.tototoshi.slick.MySQLJodaSupport._
import models.paging.Page


case class Merchant(id: Option[Long], name: String, description: String,site: String) {
  lazy val logo = "http://80.241.38.38:9000/img/merchant/" + id.get +".png"
}

/**
 * Created by irybakov on 5/25/14.
 */
class Merchants(tag: Tag) extends Table[Merchant](tag, "MERCHANT") {
  def id = column[Long]("id", O.PrimaryKey, O.AutoInc)
  def name = column[String]("name", O.NotNull)
  def description = column[String]("description", O.NotNull)
  def site = column[String]("site", O.Nullable)

  def * = (id.?, name,description,site) <> (Merchant.tupled, Merchant.unapply)

}


object MerchantsService {
  //create an instance of the table
  val merchants = TableQuery[Merchants] //see a way to architect your app in the computers-database-slick sample

  def all()(implicit s: Session): List[Merchant] = {merchants.list}

  def findById(id: Long)(implicit s: Session): Merchant = {
    val proposalByID  = merchants.withFilter(_.id === id)
    proposalByID.first()
  }

  def insert(merchant: Merchant)(implicit s: Session) {
    merchants.insert(merchant)
  }

  /**
   * Count all proposals without a filter
   */
  def count()(implicit s: Session): Int =
    Query(merchants.length).first


  def list(page: Int = 0, pageSize: Int = 10, orderBy: Int = 1, filter: String = "%")(implicit s: Session): Page[Merchant] = {

    val offset = pageSize * page

    val query =
      (for {
        merchant <- merchants
      } yield (merchant))
        .drop(offset)
        .take(pageSize)

    val totalRows = count()
    val pageCount = abs(totalRows / pageSize) + 1
    val result = query.list

    Page(result, page, offset, pageCount, totalRows)
  }
}