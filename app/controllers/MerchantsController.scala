package controllers

import models.{Proposal, MerchantsService, Merchants, Merchant}
import play.api.mvc._
import play.api.libs.json.{JsValue, Json}
import play.api.libs.json.Json._
import play.api.db.slick._
import play.api.db.slick.DBAction
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.format.Formats._

/**
 * Created by irybakov on 5/26/14.
 */
object MerchantsController extends Controller {

  implicit val merchantFormat = Json.format[Merchant]

  val merchantForm = Form(
    mapping(
      "id" -> optional(of[Long]),
      "name" -> text(),
      "description" -> text(),
      "site" -> text()
    )(Merchant.apply)(Merchant.unapply)
  )

  def insertMerchant = DBAction { implicit rs =>
    val merchant = merchantForm.bindFromRequest.get
    MerchantsService.insert(merchant)
    Redirect(routes.MerchantsController.listMerchants(0,2,""))
  }

  def merchantToJson(merchant: Merchant): JsValue = {
    val json = toJson(
      Map(
            "id" -> toJson(merchant.id),
            "name" -> toJson(merchant.name),
            "description" -> toJson(merchant.description),
            "site" -> toJson(merchant.site),
            "logo" -> toJson(merchant.logo)
        )
      )
    json
  }

  def merchantsToJson(merchantList: List[Merchant]): JsValue = {
    val json = Json.toJson(
      merchantList.map { m => merchantToJson(m) }
    )
    json
  }


  def listMerchants(page: Int, orderBy: Int, filter: String) = DBAction { implicit rs =>
    Ok(views.html.merchants(
      MerchantsService.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%"))
    ))
  }

  def getMerchants() = DBAction { implicit rs =>
    Ok(merchantsToJson(MerchantsService.all()))
  }

  def getMerchant(id: Long) = DBAction { implicit rs =>
    Ok(merchantToJson(MerchantsService.findById(id)))
  }

}
