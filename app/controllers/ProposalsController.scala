package controllers

import play.api.mvc._
import play.api.libs.json._
import play.api.db.slick._
import models._
import play.api.libs.json.Json._

import play.api.db.slick.DBAction
import play.api.data.Form
import play.api.data.Forms._
import play.api.data.format.Formats._
import play.api.cache.Cache
import play.api.Play.current
import org.joda.time.DateTime
import models.Merchant
import models.Proposal

object ProposalsController extends Controller {


  //JSON read/write macro
  implicit val proposalFormat = Json.format[Proposal]

  val proposalForm = Form(
    mapping(
      "id" -> optional(of[Long]),
      "subject" -> text(),
      "description" -> text(),
      "terms" -> text(),
      "discount"-> of[Int],
      "price"-> of[BigDecimal],
      "expires"-> of[DateTime],
      "fenceId"-> of[Long],
      "merchantId"-> of[Long]
    )(Proposal.apply)(Proposal.unapply)
  )

  def insert = DBAction { implicit rs =>
    val proposal = proposalForm.bindFromRequest.get
    ProposalsService.insert(proposal)
    Redirect(routes.ProposalsController.list(0,2,""))
  }

  def list(page: Int, orderBy: Int, filter: String) = DBAction { implicit rs =>
    Ok(views.html.proposals(
      ProposalsService.list(page = page, orderBy = orderBy, filter = ("%"+filter+"%"))
    ))
  }

  def index(id: Long) = DBAction { implicit rs =>

    val proposal: (Proposal,Merchant) = Cache.getOrElse[(Proposal,Merchant)]("proposal.key.".concat(id.toString)) {
      ProposalsService.findByIdwithMerchant(id)
    }

    Ok(views.html.index(proposal._1,proposal._2))
  }


  def jsonJoin = DBAction { implicit  rs =>
    val proposalList: List[(Proposal, Merchant)] = ProposalsService.join()

    val json: JsValue = proposalsToJson(proposalList)

    Ok(json)
  }

  def proposalsToJson(proposalList: List[(Proposal, Merchant)]): JsValue = {
    val json = Json.toJson(
      proposalList.map { p => proposalToJson(p) }
    )
    json
  }

  def proposalToJson(proposal: (Proposal, Merchant)): JsValue = {
    val json = toJson(
        Map("id" -> toJson(proposal._1.id),
          "subject" -> toJson(proposal._1.subject),
          "description" -> toJson(proposal._1.description),
          "terms" -> toJson(proposal._1.terms),
          "image" -> toJson(proposal._1.image),
          "discount" -> toJson(proposal._1.discount),
          "price" -> toJson(proposal._1.price),
          "expires" -> toJson(proposal._1.expires),
          "fenceId" -> toJson(proposal._1.fenceId),
          "merchantId" -> toJson(proposal._1.merchantId),
          "merchant" -> MerchantsController.merchantToJson(proposal._2)
        )
    )
    json
  }

  def jsonFindAll = DBAction { implicit rs =>
    val proposals = ProposalsService.all()
    Ok(toJson(proposals))
  }

  def getOneProposal(id: Long) = DBAction {  implicit rs =>

    val proposal: (Proposal,Merchant) = Cache.getOrElse[(Proposal,Merchant)]("proposal.key.".concat(id.toString)) {
      ProposalsService.findByIdwithMerchant(id)
    }
    render {
      case Accepts.Json() => Ok(proposalToJson(proposal))
      case Accepts.Html() => Ok(views.html.index(proposal._1,proposal._2))
    }
  }

  def getMerchantProposals(id: Long, offset: Int = 0, count: Int = 10) = DBAction { implicit rs =>
    val proposalList = ProposalsService.listByMerchant(id,offset,count)
    Ok(proposalsToJson(proposalList))
  }

}

