package controllers

import models.feed.{FeedProposals, Feed}
import org.joda.time.DateTime
import play.api.libs.json.JsValue
import play.api.libs.json.Json._
import play.api.mvc._
import play.api.db.slick._
import models._

import play.api.db.slick.DBAction
import play.api.Play.current

import scala.concurrent.{ExecutionContext, Future}
import scala.math.BigDecimal
import scala.util.{Failure, Success}
import ExecutionContext.Implicits.global


/**
 * Created by irybakov on 6/15/14.
 */
object FeedController extends Controller {

  def toFeedProposal(proposal: (Proposal, Merchant, Place)) {
    val id = java.util.UUID.randomUUID();
    var fp = Feed(id,
      "testuser",
      proposal._1.id.get,
      DateTime.now(),
      toMap(proposal._1),
      toMap(proposal._3),
      toMap(proposal._2)
    )

    println(proposal)
    FeedProposals.insertNewRecord(fp)
  }

  def toMap(cc: AnyRef) =
    (Map[String, String]() /: cc.getClass.getDeclaredFields) { (a, f) =>
      f.setAccessible(true)
      val v =  f.get(cc)
      if (v != null) {
        a + (f.getName -> v.toString)
      } else {
        a + (f.getName -> "")
      }
    }


  def registerFenceProposals(fenceId: Long) = DBAction { implicit rs =>
    val proposals = PlacesService.findByFenceId(fenceId)
    proposals.map { p => toFeedProposal(p)}
    Ok(proposals.length + " proposals found")
  }


  def proposalToJson(proposal: (Map[String, String], Map[String, String], Map[String, String])): JsValue = {
    val json = toJson(
      Map("id" -> toJson(proposal._1.get("id")),
        "subject" -> toJson(proposal._1.get("subject")),
        "description" -> toJson(proposal._1.get("description")),
        "terms" -> toJson(proposal._1.get("terms")),
        "image" -> toJson(proposal._1.get("image")),
        "discount" -> toJson(proposal._1.get("discount")),
        "price" -> toJson(proposal._1.get("price")),
        "expires" -> toJson(proposal._1.get("expires")),
        "fenceId" -> toJson(proposal._1.get("fenceId")),
        "merchantId" -> toJson(proposal._1.get("merchantId")),
        "merchant" -> toJson(proposal._2),
        "place" -> toJson(proposal._3)
      )
    )
    json
  }

  def feedtoJson(feeds: Seq[Feed]): JsValue = {
    val json = toJson(
      feeds.map { f =>
        Map("id" -> toJson(f.id.toString),
          "updated" -> toJson(f.updated),
          "user" -> toJson(f.user_id),
          "proposal" -> proposalToJson(Tuple3[Map[String, String], Map[String, String], Map[String, String]](f.proposal, f.merchant, f.place))
        )
      }
    )
    json
  }

  def listMyProposals(limit: Int) = Action.async {

    val f: Future[Seq[Feed]] = FeedProposals.getEntireTable
    f.map { feeds =>
      Ok(feedtoJson(feeds))
    }
  }
}
